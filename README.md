# Digbang's credits

> An application to simulate bank credits.

An application to simulate bank credits in which you can select the desired amount of money and the terms for the credit fees.
An open source project developed with react native.

## Build

The app has been tested on android 10.0, Pixel 2 API 29 emulator.
You can run the project with:

```
$ yarn react-native run-android
```
or
```
$ npx react-native run-android
```

## Branding

It was developed with the following RN libraries:

* "native-base": "^2.15.2"
* "typescript": "^4.2.4"
* "@react-native-community/slider": "^3.0.3"
* "react-native-linear-gradient": "^2.5.6"
* "react-native-modal": "^11.10.0"
* "react-native-vector-icons": "^8.1.0"

And it was built with:

* "react-native": "0.64.0"
* "nodeJS": "14.16.1"

Fonts used: [Montserrat][0].

![UI](/ejercicio.jpg?raw=true)

Some adjustments were made to the branding design to improve the UX-UI of the application buttons.

## [MIT][1] License 

Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
and associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
NCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


[0]: https://fonts.google.com/specimen/Montserrat
[1]: https://opensource.org/licenses/MIT
