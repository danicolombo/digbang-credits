import React, {useEffect, useState} from 'react';
import {Text, View, Input} from 'native-base';
import Slider from '@react-native-community/slider';
import AmountDisplay from './AmountDisplay';
import {Values} from '../../App';
import styles from '../styles';

interface CustomSliderProps {
    feeResultState: [number, (value: number) => void];
    setFeeList: (value: number[]) => void;
    initialValues: Values;
}

const CustomSlider: React.FC<CustomSliderProps> = ({ feeResultState, setFeeList, initialValues }) => {
    const [amount, setAmount] = useState<number>(500);
    const [term, setTerm] = useState<number>(3);
    const [feeResult, setFeeResult] = feeResultState;

    useEffect(() => {
        if (amount && term) {
            setFeeResult(amount / term);
            setFeeList(Array(term).fill((amount / term)));
        }
    }, [amount, term]);

    const handleAmount = (value: number) => {
        if (value > initialValues.minAmount || value < initialValues.maxAmount || !value) setAmount(value);
    }

    const handleTerm = (value: number) => {
        if (value > initialValues.minTerm || value < initialValues.maxTerm || !value) setTerm(value);
    }

    return (
        <>
            <View style={styles.inputContainer}>
                <Text style={[styles.appTextBold, styles.highlightButtonText]}>{"Monto total"}</Text>
                <Input
                    keyboardType="numeric"
                    value={amount ? amount.toString() : initialValues.minAmount.toString()}
                    style={[styles.appTextRegular, styles.inputStyles]}
                    onChangeText={(value: string) => handleAmount(parseInt(value))}
                />
            </View>
            <Slider
                style={{ width: 350, height: 20 }}
                minimumValue={initialValues.minAmount}
                maximumValue={initialValues.maxAmount}
                minimumTrackTintColor="#FFFFFF"
                maximumTrackTintColor="#FFFFFF"
                value={amount ? amount : initialValues.minAmount}
                onValueChange={(value) => handleAmount(value)}
                accessibilityLabel={"Monto total"}
                step={1}
            />
            <View style={styles.markLabels}>
                <Text style={styles.markLabelsText}>${initialValues.minAmount}</Text>
                <Text style={styles.markLabelsText}>${initialValues.maxAmount}</Text>
            </View>
            <View style={styles.inputContainer}>
                <Text style={[styles.appTextBold, styles.highlightButtonText]}>{"Plazo"}</Text>
                <Input
                    keyboardType="numeric"
                    value={term ? term.toString() : initialValues.minTerm.toString()}
                    style={[styles.appTextRegular, styles.inputStyles]}
                    onChangeText={(value: string) => handleTerm(parseInt(value))}
                />
            </View>
            <Slider
                style={{ width: 350, height: 20 }}
                minimumValue={initialValues.minTerm}
                maximumValue={initialValues.maxTerm}
                minimumTrackTintColor="#FFFFFF"
                maximumTrackTintColor="#FFFFFF"
                value={term ? term : initialValues.minTerm}
                onValueChange={(value) => handleTerm(value)}
                accessibilityLabel={"Plazos"}
                step={1}
            />
            <View style={styles.markLabels}>
                <Text style={styles.markLabelsText}>{initialValues.minTerm}</Text>
                <Text style={styles.markLabelsText}>{initialValues.maxTerm}</Text>
            </View>
            <View style={styles.fee}>
                <Text style={[styles.appTextBold, styles.highlightFee]}> Cuota fija por mes </Text>
                <AmountDisplay feeResult={feeResult} />
            </View>
        </>
    );
};

export default CustomSlider;