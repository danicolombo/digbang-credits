import React from 'react';
import {Button, Text} from 'native-base';
import {View} from 'react-native';
import Modal from 'react-native-modal';
import CreditModal from './CreditModal';
import DetailModal from './DetailModal';
import styles from '../styles';

const CustomModal: React.FC<{
    feeList: number[];
    confirmCreditModal: boolean;
    visibleState: [boolean, (value: boolean) => void];
}> = ({ feeList, confirmCreditModal, visibleState }) => {
    const [isModalVisible, setModalVisible] = visibleState;
    return (
        <Modal isVisible={isModalVisible}>
            <View style={styles.modalStyle}>
                {confirmCreditModal
                    ?
                    <CreditModal />
                    :
                    <DetailModal feeList={feeList} />
                }
                <Button primary style={[styles.creditButton, styles.modalButton]}
                    onPress={() => setModalVisible(false)}>
                    <Text style={[styles.appTextBold, styles.highlightButton]}>Cerrar</Text>
                </Button>
            </View>
        </Modal>
    );
};

export default CustomModal;