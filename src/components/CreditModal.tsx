import React from 'react';
import {Icon, Text} from 'native-base';
import styles from '../styles';

const CreditModal: React.FC = () => {
    return (
        <>
            <Icon name='md-checkmark-circle' style={styles.iconCheck} />
            <Text style={[styles.appTextBold, styles.modaltexTitle]}>Crédito otorgado con éxito</Text>
            <Text style={[styles.appTextRegular, styles.modaltext]}>Recibí el dinero en tu cuenta</Text>
            <Text style={[styles.appTextBold, styles.modaltexTitle]}>¿Cómo puedo finalizar la solicitud en el momento?</Text>
            <Text style={[styles.appTextRegular, styles.modaltext]}>Podés retomar el proceso enviando un email de validación. Tenés hasta 30 días para finalizarlo.</Text>
        </>
    );
};

export default CreditModal;