import React from 'react';
import {Linking} from 'react-native';
import {Button, Footer, FooterTab, Text} from 'native-base';
import styles from '../styles';

const CustomFooter: React.FC<{
    title: string;
}> = ({ title }) => {
    return (
        <Footer>
            <FooterTab>
                <Button onPress={() => Linking.openURL('https://procnedc.github.io/resume/')}>
                    <Text style={styles.appTextBold}>{title}</Text>
                </Button>
            </FooterTab>
        </Footer>
    );
};

export default CustomFooter;