import React from 'react';
import {Body, Button, Header, Icon, Left, Right, Subtitle, Title} from 'native-base';
import {Linking} from 'react-native';

const CustomHeader: React.FC<{
    title: string;
}> = ({ title }) => {
    return (
        <Header>
            <Left>
                <Button transparent
                    onPress={() => Linking.openURL('https://www.digbang.com/?lg=en')}>
                    <Icon name='navigate' />
                </Button>
            </Left>
            <Body>
                <Title>Digbang</Title>
                <Subtitle>{title}</Subtitle>
            </Body>
            <Right />
        </Header>
    );
};

export default CustomHeader;