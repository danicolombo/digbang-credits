import React from 'react';
import {List, ListItem, Text} from 'native-base';
import AmountDisplay from './AmountDisplay';
import styles from '../styles';

const DetailModal: React.FC<{
    feeList: number[];
}> = ({ feeList }) => {
    return (
        <>
            <Text style={[styles.appTextBold, styles.modaltexTitle]}>Detalle de las cuotas</Text>
            <List
                dataArray={feeList}
                keyExtractor={(item, index) => index.toString()}
                renderRow={(rowData) =>
                    <ListItem style={styles.listStyle}>
                        <AmountDisplay feeResult={rowData} />
                    </ListItem>}
            />
            <Text style={[styles.appTextRegular, styles.legals]}>Todos los costos aquí detallados son solamente a título informativo, no constituyen una oferta y pueden variar al momento de solicitarlos, sin previo aviso. Asimismo, están sujetos a políticas de mercado y/o internas de cada entidad.</Text>
        </>
    );
};

export default DetailModal;