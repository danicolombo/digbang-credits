import React from 'react';
import {Text, View} from 'native-base';
import styles from '../styles';

const Section: React.FC<{
    title: string;
}> = ({ children, title }) => {
    return (
        <View style={styles.sectionContainer}>
            <Text style={[styles.appTextBold, styles.sectionTitle]}>
                {title}
            </Text>
            {children}
        </View>
    );
};

export default Section;