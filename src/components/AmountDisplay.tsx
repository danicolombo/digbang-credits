import React from 'react';
import {Text} from 'native-base';
import styles from '../styles';

const AmountDisplay: React.FC<{
    feeResult: number;
}> = ({ feeResult }) => {
    const amountDisplay = Math.round(feeResult * 100) / 100;
    return (
        <Text style={[styles.totalText, styles.appTextBold]}>$ {amountDisplay}</Text>
    );
};

export default AmountDisplay;