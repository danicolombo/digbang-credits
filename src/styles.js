import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#034371',
    },
    sectionContainer: {
        flex: 1,
        flexGrow: 1,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    appTextRegular: {
        fontFamily: 'Montserrat-Regular',
        color: '#FFF',
    },
    appTextBold: {
        color: '#FFF',
        fontFamily: 'Montserrat-ExtraBold',
    },
    totalText: {
        fontSize: 24,
        textTransform: 'uppercase',
    },
    markLabelsText: {
        color: 'white',
        fontFamily: 'Montserrat-Regular',
        fontSize: 12,
    },
    highlightButtonText: {
        fontSize: 14,
        textTransform: 'uppercase',
        flexGrow: 1,
        alignSelf: 'center',
    },
    highlightFee: {
        fontSize: 14,
        textTransform: 'uppercase',
    },
    sectionTitle: {
        fontSize: 24,
        textAlign: 'center',
        paddingVertical: 25,
    },
    linear: {
        paddingHorizontal: 24,
        paddingVertical: 25,
        margin: 25,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
    },
    modaltexTitle: {
        fontSize: 18,
        textTransform: 'uppercase',
        textAlign: 'center',
        justifyContent: 'center',
        paddingHorizontal: 15,
        paddingVertical: 5,
    },
    modaltext: {
        fontSize: 16,
        textAlign: 'center',
        justifyContent: 'center',
        paddingHorizontal: 15,
        paddingVertical: 5,
        flexGrow: 1,
    },
    legals: {
        fontSize: 12,
        justifyContent: 'center',
        paddingHorizontal: 15,
        paddingVertical: 5,
        flexGrow: 1,
    },
    listStyle: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 10,
        flexGrow: 1,
    },
    inputStyles: {
        fontSize: 16,
        borderWidth: 2,
        borderColor: 'white',
        textAlign: 'center',
        maxWidth: 100,
    },
    itemInput: {
        flexGrow: 1,
    },
    inputContainer: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'flex-start', 
        flex: 1,
        textAlign: 'center',
    },
    markLabels: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        flex: 1,
        textAlign: 'center',
        marginBottom: 15,
    },
    iconCheck: {
        fontSize: 34, 
        color: '#FFF', 
        flex: 1, 
        textAlign: 'center', 
        marginTop: 35,
    },
    modalStyle: {
        flex: 1,
        backgroundColor: '#003B67',
        paddingHorizontal: 5,
        paddingVertical: 5,
    },
    fee: {
        width: '100%',
        textAlign: 'center',
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#00355D',
        alignItems: 'center', 
        paddingVertical: 25,
    },
    creditButton: {
        width: '100%',
        textAlign: 'center',
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 5,
        paddingVertical: 5,
    },
    creditButtonColor: {
        backgroundColor: '#17AA8D',
    },
    detailButtonColor: {
        backgroundColor: '#003B67',
    },
    modalButton: {
        justifyContent: 'flex-end',
        flex: undefined,
    },
    highlightButton: {
        fontSize: 16,
        textTransform: 'uppercase',
        textAlign: 'center',
    },
});

export default styles;
