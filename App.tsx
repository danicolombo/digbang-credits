/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useState} from 'react';
import {SafeAreaView, ScrollView, StatusBar, View} from 'react-native';
import {Container, Content, Text, Button} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import CustomHeader from './src/components/CustomHeader';
import Section from './src/components/Section';
import CustomSlider from './src/components/CustomSlider';
import CustomModal from './src/components/CustomModal';
import CustomFooter from './src/components/CustomFooter';
import styles from './src/styles';

export type Values = {
    minAmount: number,
    maxAmount: number,
    minTerm: number,
    maxTerm: number
};

const App = () => {
    const initialValues: Values = {
        minAmount: 500,
        maxAmount: 5000,
        minTerm: 3,
        maxTerm: 24
    };

    const [confirmCreditModal, setConfirmCreditModal] = useState(false);
    const [isModalVisible, setModalVisible] = useState(false);
    const [feeResult, setFeeResult] = useState<number>((initialValues.minAmount / initialValues.minTerm));
    const [feeList, setFeeList] = useState<number[]>([]);

    const toggleModal = (credit: boolean) => {
        setModalVisible(!isModalVisible);
        setConfirmCreditModal(credit);
    };

    return (
        <SafeAreaView>
            <StatusBar />
            <ScrollView
                contentInsetAdjustmentBehavior="automatic">
                <View>
                    <Container style={styles.container}>
                    <CustomHeader title={'Créditos'}/>
                    <Content>
                        <LinearGradient colors={['#00355D', '#003B67', '#034371']} style={styles.linear}>
                            <Section title="Simulá tu crédito">
                                <CustomSlider initialValues={initialValues} feeResultState={[feeResult, setFeeResult]} setFeeList={setFeeList}/>
                                <Button primary
                                    style={[styles.creditButton, styles.creditButtonColor]}
                                    onPress={() => toggleModal(true)}>
                                    <Text style={[styles.appTextBold, styles.highlightButton]}> Obtené tu crédito </Text>
                                </Button>
                                <Button primary
                                    style={[styles.creditButton, styles.detailButtonColor]}
                                    onPress={() => toggleModal(false)}>
                                    <Text style={[styles.appTextBold, styles.highlightButton]}> Detalle de cuotas </Text>
                                </Button>
                            </Section>
                        </LinearGradient>
                        <CustomModal confirmCreditModal={confirmCreditModal} feeList={feeList} visibleState={[isModalVisible, setModalVisible]}/>
                    </Content>
                    <CustomFooter title={'Desarrollado por Daniela Colombo'}/>      
                </Container>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

export default App;
